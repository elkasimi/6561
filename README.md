# 6561
my entry in the CodeCup 2016 competition that wins the first place

Please have a look at:
http://www.codecup.nl
for more information

# to build the player:

$cmake .

$make

# Ranking
http://www.codecup.nl/competition.php?comp=183

# Game rules

http://www.codecup.nl/rules_6561.php
